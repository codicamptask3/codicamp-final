            <div class="header-wrapper clearfix">
                <div class="logo-wrapper float-left">
                   <a href="index.php"><strong>Super</strong><span>duper</span></a>
                </div>
                <nav class="main-nav bkgd-white float-left">
                    <ul class="main-nav-list clearfix">
                        <li class="drop-trigger"><a href="">Men</a>
                            <div class="mega-menu clearfix">
                                <div class="mega-menu-column">
                                    <div class="set">
                                        <h4><a href="">Accessories</a></h4>
                                        <ul>
                                            <li><a href="">All</a></li>
                                            <li><a href="">Cellphones</a></li>
                                            <li><a href="">Tablets</a></li>
                                            <li><a href="">Laptops</a></li>
                                            <li><a href="">Desctops</a></li>
                                            <li><a href="">Wearables</a></li>
                                        </ul>
                                    </div>
                                    <div class="set">
                                        <h4 class="megam-section-border"><a href="">Chipsets</a></h4>
                                    </div>
                                    <div class="set">
                                        <h4><a href="">Desctop graphics</a></h4>
                                        <ul>
                                            <li><a href="">Wearables</a></li>
                                            <li><a href="">Cameras</a></li>
                                            <li><a href="">E-readers</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="mega-menu-column">
                                    <div class="set">
                                        <h4><a href="">Desctop Processors</a></h4>
                                        <ul>
                                            <li><a href="">Thegadgetz HD8000 Serias Graphics</a></li>
                                            <li><a href="">CAD & engineering</a></li>
                                            <li><a href="">Medical, Financial & Display Wall</a></li>
                                            <li><a href="">Cloud & Data Center</a></li>
                                            <li><a href="">Wearables</a></li>
                                            <li><a href="">Cameras</a></li>
                                            <li><a href="">E-readers</a></li>
                                        </ul>
                                    </div>
                                    <div class="set">
                                        <h4 class="megam-section-border"><a href="#">Developers</a></h4>
                                    </div>
                                    <div class="set">
                                        <h4 class="megam-section-border-bottom"><a href="#">Embedded</a></h4>
                                    </div>
                                </div>
                                <div class="mega-menu-column">
                                    <div class="set">
                                        <h4><a href="">Forums</a></h4>
                                    </div>
                                    <div class="set">
                                        <h4><a href="">FAQS</a></h4>
                                    </div>
                                    <div class="set">
                                        <h4><a href="">Cameras</a></h4>
                                    </div>
                                    <div class="set">
                                        <h4><a href="">Partners</a></h4>
                                    </div>
                                    <div class="set">
                                        <h4><a href="">Warrantly</a></h4>
                                        <ul>
                                            <li><a href="">Industrial Control & Automation</a></li>
                                            <li><a href="">Digital Caming</a></li>
                                            <li><a href="">Digital Signage & Retail</a></li>
                                            <li><a href="">Thin client</a></li>
                                            <li><a href="">Communications ifrastructure</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <a href="">view all &rarr;</a>
                            </div>
                        </li>
                        <li><a href="">Women</a></li>
                        <li><a href="">Accessories</a></li>
                        <li><a href="">Sale</a></li>
                        <li><a href="">Brands</a></li>
                        <li><a href="">Articles</a></li>
                        <li><a href="">Elements</a></li>
                        <li><a href="">About</a></li>
                        <li><a href="">Contact</a></li>
                    </ul>
                    <div class="search-panel bkgd-white">
                        <div class="search-panel-inner clearfix">
                            <div class="gender-select float-left">
                                <select>
                                    <option value="All">All</option>
                                    <option value="Men">Men</option>
                                    <option value="Women">Women</option>
                                    <option value="Accessories">Accessories</option>
                                    <option value="Sale">Sale</option>
                                    <option value="Brands">Brands</option>
                                    <option value="Articles">Articles</option>
                                    <option value="Elements">Elements</option>
                                </select>
                            </div>
                            <div class="search-field float-left clearfix">
                                <form class="search clearfix">
                                    <input class="float-left" type="text" name="text" placeholder="I want ____" />
                                    <button type="submit"></button>
                                </form>
                                <div class="search-results clearfix">
                                    <p class="float-left"><span>87</span> results</p>
                                    <a class="clear-results float-left" href=""><img src="img/clear_results_icon.png"></a>
                                </div>
                            </div>
                            <div class="more-links float-left">
                                <a class="login-panel float-left" href="">Log in</a>
                                <a class="wish-list float-left" href="#">Wish list(<span>0</span>)</a>
                            </div>
                        </div>
                    </div>
                </nav>
                <div class="control float-left">
                    <div class="mobile-trigger">
                        <a href=""><i class="fa fa-bars"></i> Navigation</a>
                    </div>
                    <div class="local-control bkgd-white">
                        <div class="change-local-wrapper clearfix">
                            <div class="change-currency float-left">
                                <select class="select-currency">
                                    <option value="euro">&#8364; Euro</option>
                                    <option value="pound">&#163; Pound</option>
                                    <option value="dollar">$ US Dollar</option>
                                </select>
                            </div>
                            <div class="change-language float-left">
                                <select class="select-language">
                                    <option value="en">EN</option>
                                    <option value="fr">FR</option>
                                    <option value="du">DU</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="widget-cart clearfix">
                        <div class="mobile-search">
                            <a href=""><i class="fa fa-search"></i></a>
                        </div>
                        <a href="#" class="items-cart">
                            <div class="items-cart-inner clearfix">
                                <div class="cart-text float-left">
                                    <span class="cart-amount">$945.99</span>
                                    <span class="items-counter">2 items</span>
                                </div>
                                <i class="fa fa-shopping-cart"></i>
                            </div>
                        </a>
                        <div class="cart-dropdown">
                            <div class="cart-dropd-item">
                                <div class="cart-dropd-item-wrapper clearfix">
                                    <img src="img/cart_dropdown_img.png">
                                    <div>
                                        <a href=""><h4>Helium Insulator Snowboard Jacket</h4></a>
                                        <span>$589.00</span>
                                    </div>
                                    <a href=""><img src="img/cart_dropdown_item_remove.png"></a>
                                </div>
                            </div>
                            <div class="cart-dropd-item">
                                <div class="cart-dropd-item-wrapper clearfix">
                                    <img src="img/cart_dropdown_img.png">
                                    <div>
                                        <a href=""><h4>Nsulator Snowboard Jacket</h4></a>
                                        <span>$367.95</span>
                                    </div>
                                    <a href=""><img src="img/cart_dropdown_item_remove.png"></a>
                                </div>
                            </div>
                            <div class="cart-dropd-item">
                                <div class="cart-dropd-item-wrapper clearfix">
                                    <img src="img/cart_dropdown_img.png">
                                    <div>
                                        <a href=""><h4>Insulator Jacket</h4></a>
                                        <span>$170.99</span>
                                    </div>
                                    <a href=""><img src="img/cart_dropdown_item_remove.png"></a>
                                </div>
                            </div>
                            <div class="cart-dropdown-checkout">
                                <a href="">Check out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>