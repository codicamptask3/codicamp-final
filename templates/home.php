
    <section class="new-products grid-row clearfix">
        <div class="new-item">
            <div class="clearfix">
                <h2>New products</h2>
                <div class="previous-next-page clearfix">
                <a href="" class="previous">
                    <img src="img/left_angle_bracket.png">
                </a>
                <a href="" class="next">
                    <img src="img/right_angle_bracket.png">
                </a>
                </div>
            </div>
        </div>
        <ul class="product-list clearfix">
            <li class="item grid-3">
                <a href="">
                <img src="img/breach_snowboard_jacket.jpg">
                </a>
                <div class="item-home-info clearfix">
                    <a class="name" href="">Breach Snowboard Jacket</a>
                    <span class="price">$991.35</span>
                </div>
            </li>
            <li class="item grid-3">
                <a href="">
                    <img src="img/moto_9_carbon.jpg">
                </a>
                <div class="item-home-info clearfix">
                    <a class="name" href="">Moto-9 carbon ???????</a>
                    <span class="price">$549.95</span>
                </div>
            </li>
            <li class="item grid-3">
                <a href="">
                    <img src="img/encore_snowboard_jacket.jpg">
                </a>
                <div class="item-home-info clearfix">
                    <a class="name" href="">Encore Snowboard Jacket</a>
                    <span class="price">$770.15<span class="old-price"><del>$1,270.15</del></span></span>
                </div>
            </li>
            <li class="item grid-3">
                <a href="">
                    <img src="img/tracker_snowboard.jpg">
                </a>
                <div class="item-home-info clearfix">
                    <a class="name" href="">The White Collection Tracker Snowboard</a>
                    <span class="price">$1550.35</span>
                </div>
            </li>
        </ul>
        <ul class="product-list clearfix">
            <li class="item grid-3">
                <a href="">
                    <img src="img/2L_Swash_Snowboard_Jacket.jpg">
                </a>
                <div class="item-home-info clearfix">
                    <a class="name" href="">2L Swash Snowboard Jacket</a>
                    <span class="price">$150.20</span>
                </div>
            </li>
            <li class="item grid-3">
                <a href="">
                    <img src="img/Leatt_Adventure_Body_Protector_Pressure.jpg">
                    <div class="triangle trngl-new"></div>
                    <span class="trngl-text">NEW</span>
                </a>
                <div class="item-home-info clearfix">
                    <a class="name" href="">Leatt Adventure Body Protector Pressure</a>
                    <span class="price">$179.99</span>
                </div>
            </li>
            <li class="item grid-3">
                <a href="">
                    <img src="img/encore_snowboard_jacket_1.jpg">
                    <div class="triangle trngl-hot"></div>
                    <span class="trngl-text">HOT</span>
                </a>
                <div class="item-home-info">
                    <a class="name" href="">Encore Snowboard Jacket</a>
                    <span class="price">$770.15</span>
                </div>
            </li>
            <li class="item grid-3">
                <a href="">
                    <img src="img/Oneal812_Graphic_MX_Ligthweigth_Fiberglass.jpg">
                </a>
                <div class="item-home-info clearfix">
                    <a class="name" href="">Oneal 812 Graphic MX Ligthweigth Fiberglass</a>
                    <span class="price">$288.99</span>
                </div>
            </li>
        </ul>
    </section>
    <section class="banner grid-row">
        <div class="adv-1 grid-3">
            <h3><span>THE</span> ELEVATION PROJECT</h3>
        </div>
        <div class="adv-2 grid-3">
            <h3>GET GEAR GRAB GOAL</h3>
        </div>
        <div class="adv-3 grid-6">
            <h3>HOW DO YOU <span>MEASURE UP?</span></h3>
        </div>
    </section>
    <section class="bestseller-products">
        <div class="new-item grid-row">
            <div class="clearfix">
                <h2>Bestseller products</h2>
                <div class="previous-next-page clearfix">
                <a href="" class="previous">
                    <img src="img/left_angle_bracket.png">
                </a>
                <a href="" class="next">
                    <img src="img/right_angle_bracket.png">
                </a>
                </div>
            </div>
        </div>
        <ul class="product-list grid-row">
            <li class="item grid-3">
                <a href="">
                    <img src="img/breach_snowboard_jacket.jpg">
                </a>
                <div class="item-home-info clearfix">
                    <a class="name" href="">Breach Snowboard Jacket</a>
                    <span class="price">$991.35</span>
                </div>
            </li>
            <li class="item grid-3">
                <a href="">
                    <img src="img/Glissade_Women_Hat.jpg">
                    <div class="triangle trngl-sale"></div>
                    <span class="trngl-text">SALE</span>
                </a>
                <div class="item-home-info clearfix">
                    <a class="name" href="">Glissade Women Hat</a>
                    <span class="price">$549.95<span class="old-price"><del>$648.15</del></span></span>
                </div>
            </li>
            <li class="item grid-3">
                <a href="">
                    <img src="img/661_Comp_II_Entry_Level_Full_Face_Helmet.jpg">
                </a>
                <div class="item-home-info clearfix">
                    <a class="name" href="">661 Comp II Entry Level Full Face Helmet</a>
                    <span class="price">$219.55</span>
                </div>
            </li>
            <li class="item grid-3">
                <a href="">
                    <img src="img/Spesializet_Demo_8_2014.jpg">
                    <div class="triangle trngl-hot"></div>
                    <span class="trngl-text">HOT</span>
                </a>
                <div class="item-home-info clearfix">
                    <a class="name" href="">Spesializet Demo 2014</a>
                    <span class="price">$10,110.99</span>
                </div>
            </li>
        </ul>
    </section>
    <section class="featured-product grid-row">
        <div class="new-item">
            <div class="clearfix">
                <h2>Featured product</h2>
                <div class="previous-next-page clearfix">
                <a href="" class="previous">
                    <img src="img/left_angle_bracket.png">
                </a>
                <a href="" class="next">
                    <img src="img/right_angle_bracket.png">
                </a>
                </div>
            </div>
        </div>
        <ul class="product-list">
            <li class="item grid-3">
                <a href="">
                    <img src="img/breach_snowboard_jacket.jpg">
                </a>
                <div class="item-home-info clearfix">
                    <a class="name" href="">Breach Snowboard Jacket</a>
                    <span class="price">$991.35</span>
                </div>
            </li>
            <li class="item grid-3">
                <a href="">
                    <img src="img/Men's_House_T-shirt.jpg">
                    <div class="triangle trngl-new"></div>
                    <span class="trngl-text">NEW</span>
                </a>
                <div class="item-home-info clearfix">
                    <a class="name" href="">Men's House T-shirt</a>
                    <span class="price">$49.95<span class="old-price"><del>$100.15</del></span></span>
                </div>
                <div class="featured-product-hover">
                    <div class="fph-previews-img clearfix">
                        <a href=""><div></div></a>
                        <a href=""><div></div></a>
                        <a href=""><div></div></a>
                    </div>
                    <div class="fph-img">
                        <img src="img/Men's_House_T-shirt.jpg">
                        <a href="">
                            <img src="img/left_angle_bracket.png">
                        </a>
                        <a href="">
                            <img src="img/right_angle_bracket.png">
                        </a>
                    </div>
                    <div class="fph-info">
                        <div class="fph-info-text clearfix">
                            <a class="name" href="">Men's House T-shirt <span>for men</span></a>
                            <span class="price">$49.95<span class="old-price"><del>$100.15</del></span></span>
                        </div>
                        <div class="fph-buttons clearfix">
                            <a href="">Add to cart</a>
                            <a href=""><i class="fa fa-heart"></i></a>
                            <a href=""><i class="fa fa-retweet"></i></a>
                        </div>
                    </div>
                </div>
            </li>
            <li class="item grid-3">
                <a href="">
                    <img src="img/encore_snowboard_jacket_1.jpg">
                </a>
                <div class="item-home-info clearfix">
                    <a class="name" href="">Encore Snowboard Jacket</a>
                    <span class="price">$159.15</span>
                </div>
            </li>
            <li class="item grid-3">
                <a href="">
                    <img src="img/SixSixOne_Evo_Wired_Full_Face.jpg">
                    <div class="triangle trngl-hot"></div>
                    <span class="trngl-text">HOT</span>
                </a>
                <div class="item-home-info clearfix">
                    <a class="name" href="">SixSixOne Evo Wired Full Face</a>
                    <span class="price">$240.00</span>
                </div>
            </li>
        </ul>
    </section>
    <section class="brands grid-row">
        <div class="brands-wrapper clearfix">
            <a href="">
                <img src="img/left_angle_bracket.png">
            </a>
            <div class="brand grid-3">
                <a href="">1</a>
            </div>
            <div class="brand grid-3">
                <a href="">SPESIALIZED</a>
            </div>
            <div class="brand grid-3">
                <a href="">FOX</a>
            </div>
            <div class="brand grid-3">
                <a href="">CUBE</a>
            </div>
            <a href="">
                <img src="img/right_angle_bracket.png">
            </a>
        </div>
    </section>
    <section class="info grid-row">
        <div class="about grid-6">
            <h2>About SuperDuper</h2>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p>
            <p>Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper </p>
        </div>
        <div class="twitter grid-3">
            <h2>Twitter</h2>
            <div class="before-last">
               <p>As a result of your previous recommendation :)</p>
               <span>31 minutes ago</span>
            </div>
            <div class="last-post">
                <p>Email that start with "Dear contact" make me feel so special</p>
                <span>50 minutes ago</span>
            </div>
        </div>
        <div class="facebook grid-3">
            <h2>Facebook</h2>
            <p>8,324 people like The Gadgetz</p>
            <div class="clearfix">
               <figure>
                <img src="img/boy.png">
                <figcaptio>Neo</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>Dave</figcaptio>
            </figure>
             <figure>
                <img src="img/Woman.png">
                <figcaptio>Elizabeth</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>Randy</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>David</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>John</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>Ed</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>Dan</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>Randy</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>David</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>Neo</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>Dave</figcaptio>
            </figure>
             <figure>
                <img src="img/Woman.png">
                <figcaptio>Elizabeth</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>Randy</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>David</figcaptio>
            </figure>
            </div>
        </div>
    </section>


