<?php
	$page_title = 'Home';
	include('templates/header.php');
?>
    <header class="page-header grid-row">
        <?php include('templates/main-nav.php') ?>
    </header>
    <section class="all_categories">
        <h1>ALL CATEGORIES</h1>
        <div class="grid-row">
            <div class=" shopping_options grid-3">
                <h2>Categories</h2>
                <ul class="nav-categories">
                    <li><a href="">Man</a></li>
                    <li><a href="">Woman</a></li>
                    <li><a href="">Accessories</a></li>
                    <li><a href="">Backpacks/bags</a></li>
                    <li><a href="">Clearance Lines</a></li>
                    <li><a href="">Climbing equipment</a></li>
                    <li><a href="">Sleeping Bags</a></li>
                    <li><a href="">Shoes</a></li>
                    <li><a href="">T-shirts</a></li>
                </ul>
                <h2>Brands</h2>
                <ul class="nav-brands">
                    <li><a href="">661</a></li>
                    <li><a href="">Animal</a></li>
                    <li><a href="">Bullet</a></li>
                    <li><a href="">Castelli</a></li>
                    <li><a href="">Cult</a></li>
                    <li><a href="">Duo</a></li>
                    <li><a href="">Hamax</a></li>
                    <li><a href="">Jarwire</a></li>
                    <li><a href="">Masterlock</a></li>
                    <li><a href="">Polaris</a></li>
                    <li><a href="">Profile Desing</a></li>
                    <li><a href="">Salsa</a></li>
                    <li><a href="">Sidi</a></li>
                    <li><a href="">Sportcurer</a></li>
                    <li><a href="">SwissStop</a></li>
                    <li><a href="">Topeak</a></li>
                    <li><a href="">Jvex</a></li>
                    <li><a href="">Weldtite</a></li>
                    <li><a href="">Zoggs</a></li>
                </ul>
            </div>
            <div class="product-option grid-9">
                <div class="filter-upper grid-row">
                    <span class="icons grid-4">
                        <a class="apps" href="https://www.google.bg/intl/bg/options/">
                            <img src="img/3x3_Active.png">
                        </a>
                        <a class="table" href="">
                            <img src="img/Table.png">
                        </a>
                    </span>
                    <div class="sort-by grid-4">
                        <span><h4>Sort by</h4></span>
                        <select class="sort-by-price">
                            <option value="price">price</option>
                            <option value="0-50">0-50</option>
                            <option value="50-100">50-100</option>
                            <option value="100-500">100-500</option>
                        </select>
                    </div>
                    <div class="show-per-page clearfix">
                        <span><h4>Show</h4></span>
                        <select class="per-page">
                            <option value="15">15</option>
                            <option value="30">30</option>
                            <option value="50">50</option>
                        </select>
                        <span><h4 class="per">per page</h4></span>
                    </div>
                </div>
                <div class="category grid-row">
                    <article class="item grid-4 clearfix">
                        <a href="">
                            <img src="img/men_1.jpg" alt="men">
                            <div class="products-info clearfix">
                                <h4 class="item-name">Men</h4>
                                <p class="quantity">2,600 items</p>
                            </div>
                        </a>
                    </article>
                    <article class="item grid-4 clearfix">
                        <a href="">
                            <img src="img/women_1.jpg" alt="women">
                            <div class="products-info clearfix">
                                <h4 class="item-name">Women</h4>
                                <p class="quantity">1,900 items</p>
                            </div>
                        </a>
                    </article>
                    <article class="item grid-4 clearfix">
                        <a href="">
                            <img src="img/Accessories.jpg" alt="Accessories">
                            <div class="products-info clearfix">
                                <h4 class="item-name">Accessories</h4>
                                <p class="quantity">859 items</p>
                            </div>
                        </a>
                    </article>
                </div>
                <div class="category grid-row">
                    <article class="item grid-4 clearfix">
                        <a href="">
                            <img src="img/backpack.jpg" alt="backpack">
                            <div class="products-info clearfix">
                                <h4 class="item-name">Backpacks/bags</h4>
                                <p class="quantity">519 items</p>
                            </div>
                        </a>
                    </article>
                    <article class="item grid-4 clearfix">
                        <a href="">
                            <img src="img/Crearance_Lines.jpg" alt="Crearance_Lines">
                            <div class="products-info clearfix">
                                <h4 class="item-name">Crearance Lines</h4>
                                <p class="quantity">321 items</p>
                            </div>
                        </a>
                    </article>
                    <article class="item grid-4 clearfix">
                        <a href="">
                            <img src="img/climbing-equipment.jpg" alt="climbing-equipment">
                            <div class="products-info clearfix">
                                <h4 class="item-name second-row">Climbing</br > Equipment</h4>
                                <p class="quantity">499 items</p>
                            </div>
                        </a>
                    </article>
                </div>
                <div class="category grid-row">
                    <article class="item grid-4 clearfix">
                        <a href="">
                            <img src="img/Sleeping_Bags.jpg" alt="Sleeping_Bags">
                            <div class="products-info clearfix">
                                <h4 class="item-name">Sleeping Bags</h4>
                                <p class="quantity">331 items</p>
                            </div>
                        </a>
                    </article>
                    <article class="item grid-4 clearfix">
                        <a href="">
                            <img src="img/Shoes.jpg" alt="Shoes">
                            <div class="products-info clearfix">
                                <h4 class="item-name">Shoes</h4>
                                <p class="quantity">259 items</p>
                            </div>
                        </a>
                    </article>
                    <article class="item grid-4 clearfix">
                        <a href="">
                            <img src="img/T-shirts.jpg" alt="T-shirts">
                            <div class="products-info clearfix">
                                <h4 class="item-name">T-shirts</h4>
                                <p class="quantity">1,890 items</p>
                            </div>
                        </a>
                    </article>
                </div>
                <div class="filter-down grid-row">
                    <span class="icons grid-4">
                        <a class="apps" href="https://www.google.bg/intl/bg/options/">
                            <img src="img/3x3_Active.png">
                        </a>
                        <a class="table" href="">
                            <img src="img/Table.png">
                        </a>
                    </span>
                    <div class="sort-by grid-4">
                        <span><h4>Sort by</h4></span>
                        <select class="sort-by-price">
                            <option value="price">price</option>
                            <option value="0-50">0-50</option>
                            <option value="50-100">50-100</option>
                            <option value="100-500">100-500</option>
                        </select>
                    </div>
                    <div class="show-per-page clearfix">
                        <span><h4>Show</h4></span>
                        <select class="per-page">
                            <option value="15">15</option>
                            <option value="30">30</option>
                            <option value="50">50</option>
                        </select>
                        <span><h4 class="per">per page</h4></span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="featured-product grid-row">
        <div class="new-item">
            <div class="clearfix">
                <h2>Featured product</h2>
                <div class="previous-next-page clearfix">
                <a href="" class="previous">
                    <img src="img/left_angle_bracket.png">
                </a>
                <a href="" class="next">
                    <img src="img/right_angle_bracket.png">
                </a>
                </div>
            </div>
        </div>
        <ul class="product-list">
            <li class="item grid-3">
                <a href="">
                    <img src="img/breach_snowboard_jacket.jpg">
                </a>
                <div class="item-home-info clearfix">
                    <a class="name" href="">Breach Snowboard Jacket</a>
                    <span class="price">$991.35</span>
                </div>
            </li>
            <li class="item grid-3">
                <a href="">
                    <img src="img/Men's_House_T-shirt.jpg">
                    <div class="triangle trngl-new"></div>
                    <span class="trngl-text">NEW</span>
                </a>
                <div class="item-home-info clearfix">
                    <a class="name" href="">Men's House T-shirt</a>
                    <span class="price">$49.95<span class="old-price"><del>$100.15</del></span></span>
                </div>
            </li>
            <li class="item grid-3">
                <a href="">
                    <img src="img/encore_snowboard_jacket_1.jpg">
                </a>
                <div class="item-home-info clearfix">
                    <a class="name" href="">Encore Snowboard Jacket</a>
                    <span class="price">$159.15</span>
                </div>
            </li>
            <li class="item grid-3">
                <a href="">
                    <img src="img/SixSixOne_Evo_Wired_Full_Face.jpg">
                    <div class="triangle trngl-hot"></div>
                    <span class="trngl-text">HOT</span>
                </a>
                <div class="item-home-info clearfix">
                    <a class="name" href="">SixSixOne Evo Wired Full Face</a>
                    <span class="price">$240.00</span>
                </div>
            </li>
        </ul>
    </section>
    <section class="banner grid-row">
        <div class="adv-1 grid-3">
            <h3><span>THE</span> ELEVATION<br /> PROJECT</h3>
        </div>
        <div class="adv-2 grid-3">
            <h3>GET GEAR<br /> GRAB GOAL</h3>
        </div>
        <div class="adv-3 grid-6">
            <h3>HOW DO YOU <span>MEASURE UP?</span></h3>
        </div>
    </section>
    <section class="info grid-row">
        <div class="about grid-6">
            <h2>About SuperDuper</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Maecenas in magna mollis lectus lacinia mollis</p>
            <p>Nulla concequat massa quis enim. Maecenas ipsum metus, semper hendrerit varius mattis, congue sit amet tellus. Aliquam ullamcorper dui sed magna posuere ut elementum enim rutrum. </p>
        </div>
        <div class="twitter grid-3">
            <h2>Twitter</h2>
            <div class="before-last">
               <p>As a result of your previous recommendation :)</p>
               <span>31 minutes ago</span>
            </div>
            <div class="last-post">
                <p>Email that start with "Dear contact" make me feel so special</p>
                <span>50 minutes ago</span>
            </div>
        </div>
        <div class="facebook grid-3">
            <h2>Facebook</h2>
            <p>8,324 people like The Gadgetz</p>
            <div class="clearfix">
               <figure>
                <img src="img/boy.png">
                <figcaptio>Neo</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>Dave</figcaptio>
            </figure>
             <figure>
                <img src="img/Woman.png">
                <figcaptio>Elizabeth</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>Randy</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>David</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>John</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>Ed</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>Dan</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>Randy</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>David</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>Neo</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>Dave</figcaptio>
            </figure>
             <figure>
                <img src="img/Woman.png">
                <figcaptio>Elizabeth</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>Randy</figcaptio>
            </figure>
             <figure>
                <img src="img/boy.png">
                <figcaptio>David</figcaptio>
            </figure>
            </div>
        </div>
    </section>
<?php include('templates/footer.php') ?>