<?php
	$page_title = 'Home';
	include('templates/header.php');
?>
    <header class="page-header grid-row">
        <?php include('templates/main-nav.php') ?>
    </header>
    <section class="product-page grid-row clearfix">
        <h1>2L SWASH SNOWBOARD JACKET</h1>
        <div class="product grid-6 clearfix">
            <div class="product-img">
                <a href=""><img src="img/2l_snowboard_jacked.jpg" alt="2l_snowboard_jacked" width="568px"></a>
            </div>
            <div class="more-img clearfix">
                <div>
                    <a href=""><img src="img/2l_snowboard_jacked_1.jpg" alt="2l_snowboard_jacked" width="140px" height="115px"></a>
                </div>
                <div>
                    <a href=""><img src="img/2l_snowboard_jacked_2.jpg" alt="2l_snowboard_jacked" width="140px" height="115px"></a>
                </div>
                <div>
                    <a href=""><img src="img/2l_snowboard_jacked_3.jpg" alt="2l_snowboard_jacked" width="140px" height="115px"></a>
                </div>
                <div class="more-products-img">
                    <i class="fa fa-caret-right"></i>
                </div>
            </div>          
        </div> 
        <div class="description grid-6 clearfix">
            <div class="prices clearfix">
                <span class="real-price">$939.39</span>
                <span class="price-sale">$1299.99</span>
                <span class="price-save">save 67%</span>
                <span class="price-review">19 Reviews<i class="fa fa-long-arrow-down"></i></span>
            </div>
            <div class="some-info clearfix">
                <p>The Full-9 is a bike-tuned, ground-up, purpose-built masterpiece. Drafting from Bell's award-winning MX helmet (the Moto-9), this top-of-the-heap full-face bike helmet incorporates the most advanced and relevant safety enhancements available while packing in timely.
                <span><a href="#">read the full review</a></span></p>
            </div>
            <div class="select-size clearfix">
                <form action="select size">
                    <label for="color">Color</label><br>
                    <button class="but-color orange2"></button>
                    <button class="but-color green1"></button>
                    <button class="but-color"></button>
                    <button class="but-color blue1"></button>
                    <button class="but-color dgreen"></button>
                    <button class="but-color black2"></button>
                    <button class="but-color purple2"></button>
                </form>
            </div>
            <div class="select-size clearfix">
                <form action="select-size">
                    <label for="size">Size</label>
                    <label for="Quantity" style="margin-left: 320px">Quantity</label>
                </form>
                <form action="select-size">
                    <ul class="switch-size">
                        <li class="switch-item" tittle="xs">xs</li>
                        <li class="switch-item" tittle="s">s</li>
                        <li class="switch-item" tittle="m">m</li>
                        <li class="switch-item" tittle="l">l</li>
                        <li class="switch-item" tittle="xl" disabled="disabled">xl</li>
                        <li class="switch-item" tittle="xxl">xxl</li>
                    </ul>
                    <div class="select-quantity">
                        <form action="select-quantity">
                            <input type="submit" value="-">
                                <select name="values">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                            <input type="submit" value="+">
                        </form>
                    </div>
                </form>
            </div>
            <div class="cart-options clearfix">
                <form action="add-cart">
                    <button class="view-more">add to cart</button>
                    <button class="switch-item"><i class="fa fa-heart"></i></button>
                    <button class="switch-item"><i class="fa fa-retweet"></i></button><br>
                </form>
            </div>
            <div class="share-product1 clearfix">
                <ul class="share-product">
                    <li class="share-item">Share</li>
                    <li class="share-item"><a href="https://www.facebook.com/"><i class="fa fa-facebook"></a></i></li>
                    <li class="share-item"><a href="https://twitter.com/"><i class="fa fa-twitter"></a></i></li>
                    <li class="share-item"><a href="https://youtube.com/"><i class="fa fa-youtube"></a></i></li>
                    <li class="share-item"><a href="https://vimeo.com/"><i class="fa fa-vimeo-square"></a></i></li>
                    <li class="share-item"><a href="https://plus.google.com/"><i class="fa fa-google-plus-square"></a></i></li>
                </ul>
            </div>
        </div>
    </section>
    <section class="grid-row">
        <div class="product-menu clearfix">
            <ul>
                <li class="active"><a href="" data-target="#tab1">Overview</a></li>
                <li><a href="" data-target="#tab2">Specificaties</a></li>
                <li><a href="" data-target="#tab3">Video's</a></li>
                <li><a href="" data-target="#tab4">Reviews</a></li>
            </ul>
        </div>
        <div id="tab1" class="tab-single-content active">
            <div class="grid-12 tab1-info">
                <div class="grid-6 left-side">
                    <h4>title</h4>
                    <div class="blue-text">
                        <p>THE DEEPEST DAY: JUSSI AND<br /> MIKEY GET IN OVER THEIR<br /> HEADS</p>
                    </div>
                    <div class="tab1-text">
                        <p>he right edge of the 928 has the classic Nokia Windows Phone button configuration: a volume rocker at top followed by a power button and a two-stage camera button (more on the camera in a bit). I’ve always loved the placement of the power button on these phones, but I’m not in love with the volume rocker — putting on the right makes it harder to hit mid-call, and it’s just barely longer than the power button which can make them difficult to differentiate by feel alone.. On the top, there’s a center-mounted Micro USB port flanked by a noise cancellation mic and a SIM tray (which, refreshingly, requires nothing more than a fingernail to pop open). At the very corner lies the 3.5mm headphone jack.</p>
                        <p>The back, though, is what sets this phone apart from the Lumia 920: You instantly notice the larger (much larger) xenon camera flash sitting just left of the lens. Another difference is the big loudspeaker grille toward the bottom, which is strategically placed on a slight</p>
                    </div>
                    <img src="img/nokia-lumia-630.jpg" alt="nokia-lumia-630" width="570px" height="550px">
                </div>
                <div class="grid-6 right-side">
                    <img src="img/Nokia-Opera-Windows-Phone.jpg" alt="Nokia-Opera-Windows-Phone" width="570px" height="680px" >
                    <div class="tab1-text">
                        <p>The first thing you’re confronted with upon obtaining one of these fancy BlackBerrys is the sheer bulk of the package they come in. Nearly the size of a shoe box, the matte black container has a high-quality soft-touch finish and a flap door held in place by magnets. It’s evidently meant to be kept around after unboxing. Inside, there’s even more blackness — the one color that pretty much never goes out of style — covering a velvety tray that houses the P‘9981 and its included dock. Lift that up and you’ll find the second tray, hosting your BlackBerry PIN card and user guides, below which resides the third level of luxurious velvet, ensconcing power adapters, cables, and a headset with in-line mic.</p>
                    </div>
                </div>
            </div>  
        </div> 
    </section> <!-- end tab1 overview -->
    <section class="grid-row">
        <div id="tab2" class="specificaties">     
           <div class="specific-line">
                <div class="grid-4 specific-col line-height">
                    <span>Product name</span>
                </div>
                <div class="grid-8 info-col line-height">
                    <span class="product-style">Breach Snowboard Jacket</span>
                </div>
            </div>
            <div class="specific-line">
                <div class="grid-4 specific-col price-height">
                    <span>Price</span>
                </div>
                <div class="grid-8 info-col">
                    <span class="price-style">$549.95</span>
                </div>
            </div>
            <div class="specific-line">
                <div class="grid-4 specific-col for-height">
                    <span>Model</span>
                </div>
                <div class="grid-8 info-col">
                    <span>Product 12390</span>
                </div>
            </div>
            <div class="specific-line">
                <div class="grid-4 specific-col for-height">
                    <span>Brand</span>
                </div>
                <div class="grid-8 info-col">
                    <span>Nike Sport</span>
                </div>
            </div>
            <div class="specific-line">
                <div class="grid-4 specific-col for-height">
                    <span>Availability</span>
                </div>
                <div class="grid-8 info-col">
                    <span class="availability-style">In Stock</span>
                </div>
            </div>
            <div class="specific-line">
                <div class="grid-4 specific-col for-height">
                    <span>Rating</span>
                </div>
                <div class="grid-8 info-col">
                    <p style="margin: 0px;"><span class="rating-style">9.7</span> based on 1 reviews.</p>
                </div>
            </div>
            <div class="specific-line">
                <div class="grid-4 specific-col summary-height">
                    <span>Summary</span>
                </div>
                <div class="grid-8 info-col">
                    <span>The Full-9 is a bike-tuned, ground-up, purpose-built masterpiece. Drafting from Bell's award-winning MX helmet (the Moto-9), this top-of-the-heap full-face bike helmet incorporates the most advanced and relevant safety enhancements available while packing in timely.
                          features like Soundtrax (Bell's built-in speaker pockets and audio cable routing), slick-integrated camera mounts, our new Overbrow Ventilation system and a full-carbon shell.
                    </span>
                </div>
            </div>
             <div class="specific-line">
                <div class="grid-4 specific-col for-height">
                    <span>Weight</span>
                </div>
                <div class="grid-8 info-col">
                    <span>10.50 kg</span>
                </div>
            </div>
            <div class="specific-line">
                <div class="grid-4 specific-col dimension">
                    <span>Dimensions (L x W x H)</span>
                </div>
                <div class="grid-8 info-col">
                    <span>150.00cm x 50.00cm x 5.00cm</span>
                </div>
                <div class="grid-8 info-col">
                    <button>Add to cart</button>
                </div>
            </div>  
        </div>
    </section> <!-- end tab2 specificaties -->
    <section>
        <div id="tab3" class="videos grid-row">
            <div class="grid-12 main-video">
                <div>
                   <iframe width="970" height="546" src="https://www.youtube.com/embed/BetpV1bbCLM" frameborder="0" allowfullscreen></iframe> 
                </div>
                <span>REVIEW 2L SWASH SNOWBOARD</span>
            </div>
            <div class="grid-12 extra-videos">
                <div class="video clearfix">
                    <div>
                        <iframe width="300" height="189" src="https://www.youtube.com/embed/nlSLaIjt1sQ?list=PLvgPRxpiEeqe883UcjrlTCR0WTThWjeii" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <span>Reviewed by Run Shmitt</span>
                </div>
                <div class="video clearfix">
                    <div>
                        <iframe width="300" height="189" src="https://www.youtube.com/embed/4zotw3CJTKQ" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <span>Test in snow</span>
                </div>
                 <div class="video clearfix">
                    <div>
                        <iframe width="300" height="189" src="https://www.youtube.com/embed/ztSeFm6pedg" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <span>Fare and water</span>
                </div>
            </div>
        </div>
    </section> <!-- end tab3 videos -->
    <section>
        <div id="tab4" class="reviews grid-row clesrfix">
            <div class="grid-3 scale">
                <div class="rating-box">
                    <span>6.1</span>
                </div>
            </div> 
            <div class="grid-9 scale-info">
                <h2>Average user reviews</h2>
                <div class="rating-line clearfix">
                    <div class="rating-1">
                        <span>1</span>
                    </div>
                    <div class="rating-2">
                        <span>2</span>
                    </div>
                    <div class="rating-3">
                        <span>3</span>
                    </div>
                    <div class="rating-4">
                        <span>4</span>
                    </div>
                    <div class="rating-5">
                        <span>5</span>
                    </div>
                    <div class=" rating-6">
                        <span>6</span>
                    </div>
                    <div class="rating-7">
                         <span>7</span>
                    </div>
                    <div class="rating-8">
                        <span>8</span>
                    </div>
                    <div class="rating-9">
                        <span>9</span>
                    </div>
                    <div class="rating-10">
                        <span>10</span>
                    </div>
                </div>
                <div class="score">
                    <div class="user-line clearfix">
                        <span class="title">Design</span>
                        <span class="number">6</span>
                        </div>
                    <div class="user-line">
                        <div class="progress-bar">
                            <span class="progres progress-6"></span>
                        </div>
                    </div>
                </div>
                <div class="score">
                    <div class="user-line clearfix">
                        <span class="title">Display Quality</span>
                        <span class="number">5</span>
                    </div>
                    <div class="user-line">
                        <div class="progress-bar">
                            <span class="progres progress-5"></span>
                        </div>
                    </div>
                </div>
                <div class="score">
                    <div class="user-line clearfix">
                        <span class="title">Software</span>
                        <span class="number">3</span>
                    </div>
                    <div class="user-line">
                        <div class="progress-bar">
                            <span class="progres progress-3"></span>
                        </div>
                    </div>
                </div>
                <div class="score">
                    <div class="user-line clearfix">
                        <span class="title">Camera(s)</span>
                        <span class="number">9</span>
                    </div>
                    <div class="user-line">
                        <div class="progress-bar">
                            <span class="progres progress-9"></span>
                        </div>
                    </div>
                </div>
                <div class="score">
                    <div class="user-line clearfix">
                        <span class="title">Battery Life</span>
                        <span class="number">10</span>
                    </div>
                    <div class="user-line">
                        <div class="progress-bar">
                            <span class="progres progress-10"></span>
                        </div>
                    </div>
                </div>
                <div class="score">
                    <div class="user-line clearfix">
                        <span class="title">Ecosystem</span>
                        <span class="number">4</span>
                    </div>
                    <div class="user-line">
                        <div class="progress-bar">
                            <span class="progres progress-4"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </section> <!-- end tab4 review -->

<?php include('templates/footer.php') ?>++