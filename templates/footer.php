        <footer class="grid-row">
            <div class="footer-inner-wrapper">
                <div class="footer-navigation clearfix">
                    <div class="grid-3">
                        <nav>
                            <h4>Category</h4>
                            <ul>
                                <li><a href="">Men</a></li>
                                <li><a href="">Woomen</a></li>
                                <li><a href="">Climbing Equipment</a></li>
                                <li><a href="">Backpacks/Bags</a></li>
                                <li><a href="">Sleeping Bags</a></li>
                                <li><a href="">Clearance Lines</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="grid-3">
                        <nav>
                            <h4>Customer Service</h4>
                            <ul>
                                <li><a href="">My Wishlist</a></li>
                                <li><a href="">My Order</a></li>
                                <li><a href="">Orders And Returns</a></li>
                                <li><a href="">Contacts</a></li>
                                <li><a href="">Site Map</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="grid-3">
                        <nav>
                            <h4>My account</h4>
                            <ul>
                                <li><a href="">About Us</a></li>
                                <li><a href="">Privacy Policy</a></li>
                                <li><a href="">Your Account</a></li>
                                <li><a href="">Address Book</a></li>
                                <li><a href="">Product Reviews</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="grid-3">
                        <div class="newsletter">
                            <h4>Newsletter</h4>
                            <p>
                                Join thousands of other people subscribe to our news
                            </p>
                            <div class="email-subscribe">
                                <form class="clearfix">
                                    <input class="float-left" type="email" placeholder="Input your email" />
                                    <button type="submit"></button>
                                </form>
                            </div>
                            <div class="social-links">
                                <ul class="clearfix">
                                    <li><a title="facebook" href=""></a></li>
                                    <li><a title="twitter" href=""></a></li>
                                    <li><a title="google-plus" href=""></a></li>
                                    <li><a title="tumblr" href=""></a></li>
                                    <li><a title="youtube" href=""></a></li>
                                    <li><a title="instagram" href=""></a></li>
                                    <li><a title="rss" href=""></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-copyright clearfix">
                    <div class="powered-by">
                        Powered with love by <a href="">Hezytheme</a>. SuperDuper — Premium Responsive Wordpress theme.
                    </div>
                    <div class="back-on-top">
                        <a href="">Top <img src="img/footer_arrow_up.png"></a>
                    </div>
                </div>
            </div>
        </footer>
    </div><!-- End of wrapper -->
    <script src="js/libs/jquery-1.8.3.min.js"></script>
    <script>
        if (typeof jQuery == 'undefined') {
            var e = document.createElement('script');
            e.src = "js/libs/jquery-1.8.3.min.js";
            e.type='text/javascript';
            document.getElementsByTagName("head")[0].appendChild(e);
        }
    </script>
    <script src="js/plugins.js"></script>
    <script src="js/application.js"></script>
</body>
</html>