<?php
	$page_title = 'Home';
	include('templates/header.php');
?>
        <header class="home-header grid-row">
            <?php include('templates/main-nav.php') ?>
            <div class="slider-wrapper grid-12">
                <div class="slider-text">
                    <h1>The Full-9 <span>is a bike-tuned</span></h1>
                    <p>Drafting from Bell's award-winning MX helmet (the Moto-9), this top-of-the-heap full-face bike helmet incorporates the most advanced and relevant safety enhancements available while packing in timely</p>
                </div>
            </div>
        </header>
<?php include('templates/home.php') ?>
<?php include('templates/footer.php') ?>