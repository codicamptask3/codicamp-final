<?php
	$page_title = 'Home';
	include('templates/header.php');
?>
    <header class="page-header grid-row">
        <?php include('templates/main-nav.php') ?>
    </header>
    <section class="categories grid-row">
        <h1><span>FOR MEN /</span>T-SHIRTS</h1>
        <p>SHOWING 1-12 OF 100 RESULDS</p>
        <div class="sd_sidebar grid-3 clearfix">
            <div class="filter">
                <div class="icons">
                    <a class="apps" href="https://www.google.bg/intl/bg/options/">
                        <img src="img/3x3_Active.png">
                    </a>
                    <a class="table">
                        <img src="img/Table.png">
                    </a>
                </div>
                <div class="sort-by">
                    <span><h4>Sort by</h4></span>
                    <select class="sort-by-price">
                        <option value="price">price</option>
                        <option value="0-50">0-50</option>
                        <option value="50-100">50-100</option>
                        <option value="100-500">100-500</option>
                    </select>
                </div>
                <div class="show-per-page clearfix">
                    <span><h4>Show</h4></span>
                    <select class="per-page">
                        <option value="15">15</option>
                        <option value="30">30</option>
                        <option value="50">50</option>
                    </select>
                    <span><h4 class="per">per page</h4></span>
                </div>
            </div>
            <div class="filter-option">
            <h2>Price</h2>
            <form>  
                <ul class="price-list">
                    <li>
                        <input type="checkbox" id="CheckBoxes"></input>
                        <label for="CheckBoxes">$0.00 - $100.00 <span>(43)<span></label><br>
                    </li>
                    <li>
                        <input type="checkbox" id="CheckBoxes1"></input>
                        <label for="CheckBoxes1">$100.00 - $200.00 <span>(27)<span></label><br>
                    </li>
                    <li>
                        <input type="checkbox" id="CheckBoxes2"></input>
                        <label for="CheckBoxes2">$100.00 - $200.00 <span>(27)<span></label><br>
                    </li>
                    <li>
                        <input type="checkbox" id="CheckBoxes3" disabled="disabled"></input>
                        <label for="CheckBoxes3">$400.00 - $90,0000 (65)</label><br>
                    </li>
                </ul>
            </form>
            </div>
            <div class="filter-option">
            <h2>Color</h2>
            <form>  
                <ul class="color-list">
                    <li>
                        <input type="checkbox" id="CheckBoxes4"></input>
                        <label for="CheckBoxes4">Brown <span>(23)<span></label><br>
                    </li>
                    <li>
                        <input type="checkbox" id="CheckBoxes5"></input>
                        <label for="CheckBoxes5">Green <span>(17)<span></label><br>
                    </li>
                    <li>
                        <input type="checkbox" id="CheckBoxes6"></input>
                        <label for="CheckBoxes6">Red <span>(45)<span></label><br>
                    </li>
                    <li>
                        <input type="checkbox" id="CheckBoxes7"></input>
                        <label for="CheckBoxes7">White <span>(30)<span></label><br>
                    </li>
                </ul>
            </form>
            </div>
            <div class="filter-option">
            <h2>Brands</h2>
            <form>  
                <ul class="brands-list">
                    <li>
                        <input type="checkbox" id="CheckBoxes8"></input>
                        <label for="CheckBoxes8">Acer <span>(23)<span></label><br>
                    </li>
                    <li>
                        <input type="checkbox" id="CheckBoxes9"></input>
                        <label for="CheckBoxes9">Aple <span>(17)<span></label><br>
                    </li>
                    <li>
                        <input type="checkbox" id="CheckBoxes10"></input>
                        <label for="CheckBoxes10">Asus <span>(45)<span></label><br>
                    </li>
                    <li>
                        <input type="checkbox" id="CheckBoxes11" disabled="disabled"></input>
                        <label for="CheckBoxes11">Dell (30)</label><br>
                    </li>
                    <li>
                        <input type="checkbox" id="CheckBoxes12" disabled="disabled"></input>
                        <label for="CheckBoxes12">Fujitsu (12)</label><br>
                    </li>
                     <li>
                        <input type="checkbox" id="CheckBoxes13"></input>
                        <label for="CheckBoxes13">Gigabyte <span>(2)<span></label><br>
                    </li>
                    <li>
                        <input type="checkbox" id="CheckBoxes14"></input>
                        <label for="CheckBoxes14">Lenovo <span>(9)<span></label><br>
                    </li>
                    <li>
                        <input type="checkbox" id="CheckBoxes15"></input>
                        <label for="CheckBoxes15">LG <span>(19)<span></label><br>
                    </li>
                    <li>
                        <input type="checkbox" id="CheckBoxes16"></input>
                        <label for="CheckBoxes16">Microsoft <span>(1)<span></label><br>
                    </li> <li>
                        <input type="checkbox" id="CheckBoxes17"></input>
                        <label for="CheckBoxes17">MSI <span>(3)<span></label><br>
                    </li>
                    <li>
                        <input type="checkbox" id="CheckBoxes18"></input>
                        <label for="CheckBoxes18">NEC <span>(5)<span></label><br>
                    </li>
                    <li>
                        <input type="checkbox" id="CheckBoxes19"></input>
                        <label for="CheckBoxes19">Nintendo <span>(10)<span></label><br>
                    </li>
                    <li>
                        <input type="checkbox" id="CheckBoxes20"></input>
                        <label for="CheckBoxes20">Olympus <span>(4)<span></label><br>
                    </li>
                     <li>
                        <input type="checkbox" id="CheckBoxes21"></input>
                        <label for="CheckBoxes21">Philips <span>(2)<span></label><br>
                    </li>
                    <li>
                        <input type="checkbox" id="CheckBoxes22"></input>
                        <label for="CheckBoxes22">Razer <span>(7)<span></label><br>
                    </li>
                </ul>
            </form>
            </div>
            <div class="filter-option">
            <h2>Size</h2>
            <form>  
                <ul class="size-list">
                    <li>
                        <input type="checkbox" id="CheckBoxes23"></input>
                        <label for="CheckBoxes23">Small <span>(1,908)<span></label><br>
                    </li>
                    <li>
                        <input type="checkbox" id="CheckBoxes24"></input>
                        <label for="CheckBoxes24">Medium <span>(5,098)<span></label><br>
                    </li>
                    <li>
                        <input type="checkbox" id="CheckBoxes25"></input>
                        <label for="CheckBoxes25">Large <span>(908)<span></label><br>
                    </li>
                    <li>
                        <input type="checkbox" id="CheckBoxes26"></input>
                        <label for="CheckBoxes26">One size <span>(100)<span></label><br>
                    </li>
                </ul>
            </form>
            </div>
        </div> <!-- sd-sidebar -->
        <div class="categories-product grid-9 clearfix" >
            <div class="product-slider grid-row clearfix">
                <div class="cat-slider grid-12 clearfix">
                    <img src="img/fleur1.jpg" alt="fleur">
                    <h2>VINTAGE<br /> FLEUR<br /> DELIS<br /> T-SHIRT</h2>
                    <div class="button-color">
                        <button class="choose-colors grey"></button>
                        <button class="choose-colors light-blue"></button>
                        <button class="choose-colors black"></button>
                        <button class="choose-colors green"></button>
                    </div> 
                </div> 
            </div>
            <div class="product-items grid-row">
                <article class="item grid-4 clearfix">
                    <a href="">
                        <img src="img/Some_image.png" alt="men" width="272.5px">
                        <div class="button-color clearfix">
                            <button class="choose-colors black"></button>
                            <button class="choose-colors gray"></button>
                            <button class="choose-colors dark-gray"></button>
                            <button class="choose-colors blue"></button>
                            <button class="choose-colors yellow"></button>
                        </div>
                        <div class="products-info clearfix">
                            <h4 class="name">Seat Belt T-shirt<br /> Men's All Over Print</h4>
                            <p class="price">$11.95</p>
                        </div>
                    </a>
                </article>
                <article class="item grid-4 clearfix">
                    <a class="status" href="">
                        <img src="img/Some_image.png" alt="men">
                        <div class="triangle trngl-hot"></div>
                        <span class="trngl-text">HOT</span>
                        <div class="button-color clearfix">
                            <button class="choose-colors black"></button>
                            <button class="choose-colors gray"></button>
                            <button class="choose-colors orange"></button>
                            <button class="choose-colors dark-gray"></button>
                            <button class="choose-colors red"></button>
                        </div>
                        <div class="products-info clearfix">
                            <h4 class="name">I Don't Even Lift<br /> T-shirt</h4>
                            <p class="price">$31.95</p>
                        </div>
                    </a>
                </article>
                <article class="item grid-4 clearfix">
                    <a href="">
                        <img src="img/Some_image.png" alt="men">
                        <div class="button-color clearfix">
                            <button class="choose-colors mavy-blue"></button>
                            <button class="choose-colors gray"></button>
                            <button class="choose-colors black"></button>
                        </div>
                        <div class="products-info clearfix">
                            <h4 class="name">Yellow thing T-shirt</h4>
                            <p class="price">$32.95</p>
                        </div>
                    </a>
                </article>
            </div>
            <div class="product-items grid-row">
                <article class="item grid-4 clearfix">
                    <a href="">
                        <img src="img/Some_image.png" alt="men">
                        <div class="button-color clearfix">
                            <button class="choose-colors black"></button>
                            <button class="choose-colors gray"></button>
                            <button class="choose-colors dark-gray"></button>
                            <button class="choose-colors blue"></button>
                            <button class="choose-colors red"></button>
                        </div>
                        <div class="products-info clearfix">
                            <h4 class="name">Breach Snowboard<br /> Jacket</h4>
                            <p class="price">$13.00</p>
                        </div>
                    </a>
                </article>
                <article class="item grid-4 clearfix">
                    <a href="">
                        <img src="img/Some_image.png" alt="men">
                        <div class="button-color clearfix">
                            <button class="choose-colors black"></button>
                            <button class="choose-colors gray"></button>
                            <button class="choose-colors yellow"></button>
                        </div>
                        <div class="products-info clearfix">
                            <h4 class="name">Vintige Fleur de lis<br /> T-shirt</h4>
                            <p class="price">$23.95</p>
                        </div>
                    </a>
                </article>
                <article class="item grid-4 clearfix">
                    <a href="">
                        <img src="img/Some_image.png" alt="men">
                        <div class="button-color clearfix">
                            <button class="choose-colors dark-gray"></button>
                            <button class="choose-colors red"></button>
                            <button class="choose-colors gray"></button>
                            <button class="choose-colors black"></button>
                            <button class="choose-colors green"></button>
                        </div>
                        <div class="products-info clearfix">
                            <h4 class="name">Turn the table</h4>
                            <p class="price">$24.67</p>
                        </div>
                    </a>
                </article>
            </div>
            <div class="product-items grid-row">
                <article class="item grid-4 clearfix">
                    <a href="">
                        <img src="img/Some_image.png" alt="men">
                        <div class="button-color clearfix">
                            <button class="choose-colors black"></button>
                            <button class="choose-colors gray"></button>
                            <button class="choose-colors dark-gray"></button>
                            <button class="choose-colors blue"></button>
                            <button class="choose-colors red"></button>
                        </div>
                        <div class="products-info clearfix">
                            <h4 class="name">Seat Belt T-shirt<br /> Men's All Over Print</h4>
                            <p class="price">$11.95</p>
                        </div>
                    </a>
                </article>
                <article class="item grid-4 clearfix">
                    <a href="">
                        <img src="img/Some_image.png" alt="men">
                        <div class="button-color clearfix">
                            <button class="choose-colors black"></button>
                            <button class="choose-colors gray"></button>
                            <button class="choose-colors green"></button>
                            <button class="choose-colors blue"></button>
                            <button class="choose-colors red"></button>
                        </div>
                        <div class="products-info clearfix">
                            <h4 class="name">I Don't Even Lift<br /> T-shirt</h4>
                            <p class="price">$31.95</p>
                        </div>
                    </a>
                </article>
                <article class="item grid-4 clearfix">
                    <a class="status" href="">
                        <img src="img/Some_image.png" alt="men">
                        <div class="triangle trngl-new"></div>
                        <span class="trngl-text">NEW</span>
                        <div class="button-color clearfix">
                            <button class="choose-colors red"></button>
                            <button class="choose-colors gray"></button>
                            <button class="choose-colors black"></button>
                            <button class="choose-colors pink"></button>
                        </div>
                        <div class="products-info clearfix">
                            <h4 class="name">Breach Snowboard<br /> Jacket</h4>
                            <p class="price">$69.95</p>
                        </div>
                    </a>
                </article>
            </div>
            <div class="product-items grid-row">
                <article class="item grid-4 clearfix">
                    <a href="">
                        <img src="img/Some_image.png" alt="men">
                        <div class="button-color clearfix">
                            <button class="choose-colors pink"></button>
                            <button class="choose-colors gray"></button>
                            <button class="choose-colors black"></button>
                            <button class="choose-colors blue"></button>
                            <button class="choose-colors red"></button>
                        </div>
                        <div class="products-info clearfix">
                            <h4 class="name">Breach Snowboard<br /> Jacket</h4>
                            <p class="price">$15.95</p>
                        </div>
                    </a>
                </article>
                <article class="item grid-4 clearfix">
                    <a href="">
                        <img src="img/Some_image.png" alt="men">
                        <div class="button-color clearfix">
                            <button class="choose-colors black"></button>
                            <button class="choose-colors gray"></button>
                            <button class="choose-colors green"></button>
                            <button class="choose-colors blue"></button>
                            <button class="choose-colors red"></button>
                        </div>
                        <div class="products-info clearfix">
                            <h4 class="name">I Don't Even Lift<br /> T-shirt</h4>
                            <p class="price">$149.95</p>
                        </div>
                    </a>
                </article>
                <article class="item grid-4 clearfix">
                    <a href="">
                        <img src="img/Some_image.png" alt="men">
                        <div class="button-color clearfix">
                            <button class="choose-colors red"></button>
                            <button class="choose-colors gray"></button>
                            <button class="choose-colors black"></button>
                            <button class="choose-colors pink"></button>
                        </div>
                        <div class="products-info clearfix">
                            <h4 class="name">Vintige Fleur de lis<br /> T-shirt</h4>
                            <p class="price">$49.35</p>
                        </div>
                    </a>
                </article>
            </div>
            <div class="product-items grid-row">
                <article class="item grid-4 clearfix">
                    <a href="">
                        <img src="img/Some_image.png" alt="men">
                        <div class="button-color clearfix">
                            <button class="choose-colors black"></button>
                            <button class="choose-colors gray"></button>
                            <button class="choose-colors dark-gray"></button>
                            <button class="choose-colors mavy-blue"></button>
                        </div>
                        <div class="products-info clearfix">
                            <h4 class="name">Tennis - Weapon<br />T-shirt</h4>
                            <p class="price">$79.95</p>
                        </div>
                    </a>
                </article>
                <article class="item grid-4 clearfix">
                    <a href="">
                        <img src="img/Some_image.png" alt="men">
                        <div class="button-color clearfix">
                            <button class="choose-colors black"></button>
                            <button class="choose-colors gray"></button>
                            <button class="choose-colors green"></button>
                            <button class="choose-colors dark-gray"></button>
                            <button class="choose-colors red"></button>
                        </div>
                        <div class="products-info clearfix">
                            <h4 class="name">Vintige Fleur de lis<br /> T-shirt</h4>
                            <p class="price">$19.95</p>
                        </div>
                    </a>
                </article>
                <article class="item grid-4 clearfix">
                    <a href="">
                        <img src="img/Some_image.png" alt="men">
                        <div class="button-color clearfix">
                            <button class="choose-colors red"></button>
                            <button class="choose-colors gray"></button>
                            <button class="choose-colors black"></button>
                            <button class="choose-colors pink"></button>
                        </div>
                        <div class="products-info clearfix">
                            <h4 class="name">Breach Snowboard<br /> Jacket</h4>
                            <p class="price">$15.95</p>
                        </div>
                    </a>
                </article>
            </div>
            <div class="grid-12">
                <button class="view-more">View more<i class="fa fa-repeat"></i></button>
            </div>
        </div> <!-- categories-product -->
        
    </section>

<?php include('templates/footer.php') ?>++